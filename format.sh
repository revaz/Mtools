#!/bin/bash

autopep8 -r -j -1 --in-place --aggressive --aggressive dev Doc examples Mtools scripts/* setup.py
