#!/usr/bin/env python


from optparse import OptionParser
import Ptools as pt
from pNbody import *
from pNbody import units
import string


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser = pt.add_postscript_options(parser)
    parser = pt.add_ftype_options(parser)
    parser = pt.add_reduc_options(parser)
    parser = pt.add_center_options(parser)
    parser = pt.add_select_options(parser)
    parser = pt.add_cmd_options(parser)
    parser = pt.add_display_options(parser)
    parser = pt.add_info_options(parser)
    parser = pt.add_limits_options(parser)
    parser = pt.add_log_options(parser)

    parser.add_option("-d", "--dir",
                      action="store",
                      dest="dir",
                      type="string",
                      default=None,
                      help="output directory for png files")

    parser.add_option("--param",
                      action="store",
                      dest="param",
                      type="string",
                      default=None,
                      help="parameter file")

    parser.add_option("--x",
                      action="store",
                      dest="x",
                      type="string",
                      default='r',
                      help="x value to plot",
                      metavar=" STRING")

    parser.add_option("--y",
                      action="store",
                      dest="y",
                      type="string",
                      default='T',
                      help="y value to plot",
                      metavar=" STRING")

    parser.add_option("--z",
                      action="store",
                      dest="z",
                      type="string",
                      default=None,
                      help="z value to plot",
                      metavar=" STRING")

    parser.add_option("--xlabel",
                      action="store",
                      dest="xlabel",
                      type="string",
                      default=None,
                      help="xlabel",
                      metavar=" STRING")

    parser.add_option("--ylabel",
                      action="store",
                      dest="ylabel",
                      type="string",
                      default=None,
                      help="ylabel",
                      metavar=" STRING")

    parser.add_option("-n",
                      action="store",
                      dest="n",
                      type="int",
                      default=1,
                      help="number of images",
                      metavar=" INTEGER")

    (options, args) = parser.parse_args()

    files = args

    return files, options


#######################################
# MakePlot
#######################################


def MakePlot(files, opt):

    xmin = opt.xmin
    xmax = opt.xmax
    ymin = opt.ymin
    ymax = opt.ymax

    # figure
    fig = pt.gcf()
    ax = pt.gca()

    # here, we run the parameter file
    if opt.param is not None:
        exec(compile(open(opt.param).read(), opt.param, 'exec'))

    # plot axis
    pt.SetAxis(xmin, xmax, ymin, ymax, log=opt.log)

    if opt.xlabel is not None:
        pt.xlabel(opt.xlabel, fontsize=pt.labelfont)
    if opt.ylabel is not None:
        pt.ylabel(opt.ylabel, fontsize=pt.labelfont)

    pt.grid(False)


########################################################################
# MAIN
########################################################################


if __name__ == '__main__':
    files, opt = parse_options()

    # (500*(1+0.1))/100

    if opt.dir is not None:
        if not os.path.exists(opt.dir):
            os.mkdir(opt.dir)

    opt.size = (5.12, 5.12)

    # create directory

    for i in range(opt.n):

        if opt.dir is not None:
            opt.ps = os.path.join(opt.dir, "%08d.png" % i)
            print((opt.ps))

        # counter
        opt.i = i

        pt.InitPlot(files, opt)
        pt.pcolors
        MakePlot(files, opt)
        pt.EndPlot(files, opt)
