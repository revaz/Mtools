#!/usr/bin/env python
'''
 @package   Mtools
 @file      setup.py
 @brief     Install Mtools
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of Mtools.
'''

from setuptools import setup, find_packages
from glob import glob

# DO NOT TOUCH BELOW

# scripts to install
scripts = glob("scripts/*")

# data

setup(
    name="Mtools",
    author="Yves Revaz",
    author_email="yves.revaz@epfl.ch",
    url="http://obswww.unige.ch/~revaz",
    description="""Mtools module""",
    license="GPLv3",
    version="0.0",

    packages=find_packages(),
    scripts=scripts,
    install_requires=["pNbody", "Ptools", "Pillow","astropy", "numpy"],
    
    
    
)
