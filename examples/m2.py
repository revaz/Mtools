# -*- coding: iso-8859-1 -*-


mov = Movie(fps=24, outputdirectory=opt.outputdirectory, size=(512, 512))


mov.setshading((0.5, 0.5))
mov.setimage('title1.png')
mov.wait(duration=1.5)


mov.setshading((3, 0))
mov.addimages('left')

mov.settransition(duration=1.5, type='default')

mov.setshading((0, 1))
mov.setimage('title2.png')
mov.wait(duration=3)
