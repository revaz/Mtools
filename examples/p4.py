# -*- coding: iso-8859-1 -*-

img = imgs[0]

imga = img_crop(img, pos=(96, 96), size=(64, 64))
imga = img_resize(imga, size=(96, 96))
img = img_resize(img, size=(384, 384))
img = img_add_thumbnail(img, imga, pos=(0, 0))
