# -*- coding: iso-8859-1 -*-


imga = img_crop(imgs[0], pos=(96, 96), size=(64, 64))
imga = img_resize(imga, (50, 50))

imgb = img_crop(imgs[1], pos=(96, 96), size=(64, 64))
imgb = img_resize(imgb, (50, 50))


img = img_append(imgs, location='horizontal')

# now add top border
img = img_add_borders(img, color=(0, 0, 0), location='top', width=50)


# add time
txt = "time = %d Myr" % i
img = img_add_text(
    img,
    txt,
    color=(
        255,
        255,
        255),
    font="/home/revaz/pgm/python/pNbody-4.0/fonts/Courier_New_Bold.ttf",
    size=20,
    center='both',
    cbox=(
        0,
        0,
        512,
        50))


# add thumb

imgs = img_add_thumbnail(img, imga, pos=(0, 0))
imgs = img_add_thumbnail(img, imgb, pos=(512 - 50, 0))
