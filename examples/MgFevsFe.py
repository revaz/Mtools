
size = (1.0, 1.0)

# Fe
xmin = -4.0
xmax = 0.0
nbfc.pos[:, 0] = (nbfc.Fe() - xmin) / (xmax - xmin) * (2. * size[0]) - size[0]
# MgFe
ymin = -1.0
ymax = 1.0
nbfc.pos[:, 1] = (nbfc.MgFe() - ymin) / (ymax - ymin) * \
    (2. * size[0]) - size[1]


nbfc.rsp = ones(nbfc.nbody, float32)
