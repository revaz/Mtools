#!/usr/bin/env python


from Mtools import *
from Mtools import pyfits

from pNbody.libutil import set_ranges


files = []
args = []


files.append('gas.fits')
dict = {}
dict['f'] = 1.0
dict['ar'] = 58.
dict['ag'] = 119.
dict['ab'] = 255.
dict['mn'] = 0.
dict['mx'] = 0.
dict['cd'] = 0.
dict['scale'] = 'log'
args.append(dict)


files.append('stars.fits')
dict = {}
dict['f'] = 1.0
dict['ar'] = 255.
dict['ag'] = 0.
dict['ab'] = 0.
dict['mn'] = 0.
dict['mx'] = 0.
dict['cd'] = 0.
dict['scale'] = 'log'
args.append(dict)


files.append('halo.fits')
dict = {}
dict['f'] = 0.5
dict['ar'] = 0.
dict['ag'] = 153.
dict['ab'] = 54.
dict['mn'] = 0.
dict['mx'] = 0.
dict['cd'] = 0.
dict['scale'] = 'log'
args.append(dict)


img = fits_compose_colors_img(files, args)

img.save('qq.png')
