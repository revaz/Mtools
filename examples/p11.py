# -*- coding: iso-8859-1 -*-

img = imgs[0]


img_w = img.size[0]
img_h = int(img_w * 3 / 4.)
border_h = (img_h - img.size[1]) / 2

# add top border
img = img_add_borders(img, color=(0, 0, 0), location='top', width=border_h)
img = img_add_borders(
    img,
    color=(
        0,
        0,
        0),
    location='bottom',
    width=border_h +
    1)
