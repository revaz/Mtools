#!/usr/bin/env python


from Mtools import *
from Mtools import pyfits

from pNbody.libutil import set_ranges


def normalize(mat, scale='log', mn=None, mx=None, cd=None):
    '''
    Transform an n x m float array into an n x m int array that will be
    used to create an image. The float values are rescaled and cutted in order to range
    between 0 and 255.

    mat   : the matrice
    scale	: lin or log
    mn  	: lower value for the cutoff
    mx 	: higer value for the cutoff
    cd	: parameter

    '''

    rm = ravel(mat)

    if mn is None:
        mn = min(rm)
    if mx is None:
        mx = max(rm)

    if mn == mx:
        mn = min(rm)
        mx = max(rm)

    mat = clip(mat, mn, mx)

    if scale == 'log':

        if cd is None or cd == 0:
            cd = rm.mean()

        try:
            mat = log(1. + (mat - mn) / (cd)) / log(1. + (mx - mn) / (cd))
        except BaseException:
            mat = mat * 0.

    elif scale == 'lin':

        mat = (mat - mn) / (mx - mn)
        cd = 0

    return mat, mn, mx, cd


file0 = 'gas.fits'
file1 = 'stars.fits'
file2 = 'halo.fits'

####################
# open files
####################

fitsimg0 = pyfits.open(file0)
fitsimg1 = pyfits.open(file1)
fitsimg2 = pyfits.open(file2)

data_0 = fitsimg0[0].data
data_1 = fitsimg1[0].data
data_2 = fitsimg2[0].data

size = (data_0.shape[1], data_0.shape[0])


####################
# compose
####################

# comp0
f0 = 1.
ar0 = 58.
ag0 = 119.
ab0 = 255.
mn0 = 0.
mx0 = 0.
cd0 = 0.
scale0 = 'log'

# comp1
f1 = 1.
ar1 = 255
ag1 = 0.
ab1 = 0.
mn1 = 0.
mx1 = 0.
cd1 = 0.
scale1 = 'log'

# comp2
f2 = 0.5
ar2 = 0.
ag2 = 153.
ab2 = 54.
mn2 = 0.
mx2 = 0.
cd2 = 0.
scale2 = 'log'


# normalize weights
n0 = sqrt(ar0**2 + ag0**2 + ab0**2)
ar0 = ar0 / n0 * 255. * f0
ag0 = ag0 / n0 * 255. * f0
ab0 = ab0 / n0 * 255. * f0

n1 = sqrt(ar1**2 + ag1**2 + ab1**2)
ar1 = ar1 / n1 * 255. * f1
ag1 = ag1 / n1 * 255. * f1
ab1 = ab1 / n1 * 255. * f1

n2 = sqrt(ar2**2 + ag2**2 + ab2**2)
ar2 = ar2 / n2 * 255. * f2
ag2 = ag2 / n2 * 255. * f2
ab2 = ab2 / n2 * 255. * f2


# 1 on normalise les cartes
# de 0 a 1

data_0, tmp_mn, tmp_mx, tmp_cd = normalize(
    data_0, scale=scale0, mn0=0., mx0=0., cd0=0.)
data_1, tmp_mn, tmp_mx, tmp_cd = normalize(
    data_1, scale=scale1, mn1=0., mx1=0., cd1=0.)
data_2, tmp_mn, tmp_mx, tmp_cd = normalize(
    data_2, scale=scale2, mn2=0., mx2=0., cd2=0.)


# add

r = ar2 * data_2 + ar0 * data_0 + ar1 * data_1
g = ag2 * data_2 + ag0 * data_0 + ag1 * data_1
b = ab2 * data_2 + ab0 * data_0 + ab1 * data_1


r = uint8(clip(r, 0, 255))
g = uint8(clip(g, 0, 255))
b = uint8(clip(b, 0, 255))

r = transpose(r)
g = transpose(g)
b = transpose(b)


image_r = Image.fromstring("L", size, r)
image_g = Image.fromstring("L", size, g)
image_b = Image.fromstring("L", size, b)

img = Image.merge("RGB", (image_r, image_g, image_b))

img.save('qq.png')
