# -*- coding: iso-8859-1 -*-


mov = Movie(fps=24, outputdirectory=opt.outputdirectory, size=(512, 512))
mov.setimage('title1.png')
mov.setshading((2, 2))
mov.wait(duration=5)


mov.setshading(None)
mov.move(speedx=128 / 2., duration=2)
mov.move(speedx=-128 / 2., duration=2)

mov.setshading((0, 1))
mov.rotate(speed=360 / 4., duration=4)

mov.setshading((4, 2))
mov.addimages('left')


mov.setimage('title2.png')
mov.setshading((1, 1))
mov.wait(duration=3)
