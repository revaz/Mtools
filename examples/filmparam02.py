################################
# film parameters
################################

#global film

film = {}
film['ftype'] = "gadget"
film['time'] = "nb.atime"
film['exec'] = 'nb.histocenter(rbox=10)'
film['macro'] = None
film['frames'] = []

frsp = 3.

######################
# a frame
######################

frame = {}
frame['width'] = 512
frame['height'] = 512
frame['view'] = 'xy'
frame['tdir'] = None
frame['pfile'] = None
frame['exec'] = """nbf = nbf.select('gas')"""
frame['macro'] = None
frame['components'] = []


# component '
component = {}
component['id'] = '#gasMgFevsFe'
component['size'] = (1.0, 1.0)
component['rendering'] = 'map'
component['mode'] = 'm'
component['exec'] = None
component['macro'] = 'MgFevsFe.py'
component['frsp'] = frsp
component['filter_name'] = None
frame['components'].append(component)


# append to film
film['frames'].append(frame)


######################
# a frame
######################

frame = {}
frame['width'] = 512
frame['height'] = 512
frame['view'] = 'xy'
frame['tdir'] = None
frame['pfile'] = None
frame['exec'] = """nbf = nbf.select('stars1')"""
frame['macro'] = None
frame['components'] = []


# component '
component = {}
component['id'] = '#starsMgFevsFe'
component['size'] = (1.0, 1.0)
component['rendering'] = 'map'
component['mode'] = 'm'
component['exec'] = None
component['macro'] = 'MgFevsFe.py'
component['frsp'] = frsp
component['filter_name'] = None
frame['components'].append(component)


# append to film
film['frames'].append(frame)
