# -*- coding: iso-8859-1 -*-

img = imgs[0]

palette_h = 54
img_w = img.size[0]

# add top border
img = img_add_borders(img, color=(64, 0, 0), location='top', width=palette_h)

# add top border
img = img_add_thumbnail(img, 'palette.png', size=(img_w, palette_h))

# add text
img = img_add_text(
    img,
    "gas temperature",
    color=(
        256,
        0,
        0),
    font="/home/revaz/pgm/python/pNbody-4.0/fonts/Courier_New_Bold.ttf",
    size=20,
    center='both',
    cbox=(
        0,
        0,
        img_w,
        palette_h))


# add text
img = img_add_text(
    img,
    "Revaz 2010",
    color=(
        0,
        0,
        0),
    font="/home/revaz/pgm/python/pNbody-4.0/fonts/Courier_New_Bold.ttf",
    size=16,
    pos=(
        140,
        280))


# add text
txt = "time = %d Myr" % i
img = img_add_text(
    img,
    txt,
    color=(
        0,
        0,
        0),
    font="/home/revaz/pgm/python/pNbody-4.0/fonts/Courier_New_Bold.ttf",
    size=16,
    pos=(
        10,
        72))
