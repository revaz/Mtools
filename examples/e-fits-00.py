#!/usr/bin/env python


from Mtools import *
from Mtools import pyfits


file0 = 'gas.fits'
file1 = 'stars.fits'
file2 = 'halo.fits'

####################
# open files
####################

fitsimg0 = pyfits.open(file0)
fitsimg1 = pyfits.open(file1)
fitsimg2 = pyfits.open(file2)

data_0 = fitsimg0[0].data
data_1 = fitsimg1[0].data
data_2 = fitsimg2[0].data

size = (data_0.shape[1], data_0.shape[0])


####################
# compose
####################

# comp0
f0 = 1e4
ar0 = 58.
ag0 = 119.
ab0 = 255.

# comp1
f1 = 1e4
ar1 = 255
ag1 = 0.
ab1 = 0.

# comp2
f2 = 1e3
ar2 = 0.
ag2 = 153.
ab2 = 54.


#
mx = 0.5		# max for 255
fmx = 0.001		# relative turnoff


cd = fmx * mx
cte = 255 / log(1. + mx / cd)


ar0 = ar0 * f0
ag0 = ag0 * f0
ab0 = ab0 * f0

ar1 = ar1 * f1
ag1 = ag1 * f1
ab1 = ab1 * f1

ar2 = ar2 * f2
ag2 = ag2 * f2
ab2 = ab2 * f2


r = ar2 * data_2 + ar0 * data_0 + ar1 * data_1
g = ag2 * data_2 + ag0 * data_0 + ag1 * data_1
b = ab2 * data_2 + ab0 * data_0 + ab1 * data_1


r = cte * log(1. + r / cd)
g = cte * log(1. + g / cd)
b = cte * log(1. + b / cd)


r = uint8(clip(r, 0, 255))
g = uint8(clip(g, 0, 255))
b = uint8(clip(b, 0, 255))

r = transpose(r)
g = transpose(g)
b = transpose(b)


image_r = Image.fromstring("L", size, r)
image_g = Image.fromstring("L", size, g)
image_b = Image.fromstring("L", size, b)

img = Image.merge("RGB", (image_r, image_g, image_b))

img.save('qq.png')
