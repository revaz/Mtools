################################
# film parameters
################################

#global film

film = {}
film['ftype'] = "gadget"
film['time'] = "nb.atime"
film['exec'] = """nb.histocenter()"""
film['macro'] = None
film['frames'] = []


frsp = 10.
size = (3, 3)


######################
# a frame (composition)
######################

frame = {}
frame['width'] = 512
frame['height'] = 512
frame['view'] = 'xy'
frame['tdir'] = None
frame['pfile'] = None
frame['exec'] = None
frame['macro'] = None
frame['components'] = []
frame['cargs'] = []


# component '
component = {}
component['id'] = 'gas'
component['size'] = size
component['rendering'] = 'map'
component['mode'] = 'm'
component['exec'] = None
component['macro'] = None
component['frsp'] = 5.
component['filter_name'] = None
frame['components'].append(component)


# component '
component = {}
component['id'] = 'stars1'
component['size'] = size
component['rendering'] = 'map'
component['mode'] = 'm'
component['exec'] = None
component['macro'] = None
component['frsp'] = 3.
component['filter_name'] = None
frame['components'].append(component)


# component '
component = {}
component['id'] = 'halo1'
component['size'] = size
component['rendering'] = 'map'
component['mode'] = 'm'
component['exec'] = None
component['macro'] = None
component['frsp'] = 3.
component['filter_name'] = None
frame['components'].append(component)


# append to film
film['frames'].append(frame)


######################
# a frame (gas)
######################

frsp = 10.

frame = {}
frame['width'] = 512
frame['height'] = 512
frame['view'] = 'xy'
frame['tdir'] = None
frame['pfile'] = None
frame['exec'] = """nbf = nbf.select('gas');nbf.rotate(angle=-45*pi/180,axis='z');nbf.rotate(angle=-40*pi/180,axis='y');nbf = nbf.selectc(fabs(nbf.z())<0.1)"""
frame['macro'] = None
frame['components'] = []


# component '
component = {}
component['id'] = '#gas-T'
component['size'] = size
component['rendering'] = 'map'
component['mode'] = 'log10(nb.T())'
component['exec'] = None
component['macro'] = None
component['frsp'] = frsp
component['filter_name'] = None
frame['components'].append(component)


# component '
component = {}
component['id'] = '#gas-Fe'
component['size'] = size
component['rendering'] = 'map'
component['mode'] = 'nb.Fe()'
component['exec'] = None
component['macro'] = None
component['frsp'] = frsp
component['filter_name'] = None
frame['components'].append(component)


# component '
component = {}
component['id'] = '#gas-MgFe'
component['size'] = size
component['rendering'] = 'map'
component['mode'] = 'nb.MgFe()'
component['exec'] = None
component['macro'] = None
component['frsp'] = frsp
component['filter_name'] = None
frame['components'].append(component)


# append to film
film['frames'].append(frame)
