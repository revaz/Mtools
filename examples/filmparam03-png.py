################################
# film parameters
################################

#global film

film = {}
film['ftype'] = "gadget"
film['time'] = "nb.atime"
film['exec'] = """nb.histocenter()"""
film['macro'] = None
film['format'] = '-'
film['frames'] = []

# compose different frames
film['img_cmd'] = []

film['img_cmd'].append(
    """text = r"$\\rm{Time= %5.2f Gyr}$"%(imgs[1].atime*4.7/1000)""")
film['img_cmd'].append(
    """ax = mt.img_mplmkaxes(img,xmin=-3,xmax=3,ymin=-3,ymax=3,xlabel=None,ylabel=None,figsize=(512,512),text=text,x=0,y=2.4)""")
film['img_cmd'].append(
    """imgs[1] = mt.img_add_axes(imgs[1],ax,size=(512,512))""")

film['img_cmd'].append("""palette = mt.img_open("colorbars/cb-white.png")""")
film['img_cmd'].append(
    """imgs[0] = mt.img_append([imgs[0],palette],location='vertical')""")
film['img_cmd'].append(
    """imgs[1] = mt.img_append([imgs[1],palette],location='vertical')""")

film['img_cmd'].append(
    """img1 = mt.img_append([imgs[1],imgs[2],imgs[3],imgs[0]],location='horizontal')""")
film['img_cmd'].append(
    """img2 = mt.img_append([imgs[4],imgs[5],imgs[6],imgs[7]],location='horizontal')""")
film['img_cmd'].append(
    """img  = mt.img_append([img1,img2],location='vertical')""")


frsp = 10.
size = (3, 3)

######################
# a frame (external call)
######################

frame = {}
frame['ext_cmd'] = []
frame['ext_cmd'].append("""from gasNvsFe import MakePlot""")
frame['ext_cmd'].append("""MakePlot([nbf],output)""")


# append to film
film['frames'].append(frame)


######################
# a frame (composition)
######################

frame = {}
frame['width'] = 512
frame['height'] = 512
frame['view'] = 'xy'
frame['tdir'] = None
frame['pfile'] = None
frame['exec'] = None
frame['macro'] = None
frame['components'] = []
frame['cargs'] = []


# component '
component = {}
component['id'] = 'gas'
component['size'] = size
component['rendering'] = 'map'
component['mode'] = 'm'
component['exec'] = None
component['macro'] = None
component['frsp'] = 5.
component['filter_name'] = None
frame['components'].append(component)

dict = {}
dict['f'] = 1.0
dict['ar'] = 58.
dict['ag'] = 119.
dict['ab'] = 255.
dict['mn'] = 0.
dict['mx'] = 0.
dict['cd'] = 0.
dict['scale'] = 'log'
frame['cargs'].append(dict)


# component '
component = {}
component['id'] = 'stars1'
component['size'] = size
component['rendering'] = 'map'
component['mode'] = 'm'
component['exec'] = None
component['macro'] = None
component['frsp'] = 3.
component['filter_name'] = None
frame['components'].append(component)

dict = {}
dict['f'] = 1.0
dict['ar'] = 255.
dict['ag'] = 0.
dict['ab'] = 0.
dict['mn'] = 0.
dict['mx'] = 0.
dict['cd'] = 0.
dict['scale'] = 'log'
frame['cargs'].append(dict)


# component '
component = {}
component['id'] = 'halo1'
component['size'] = size
component['rendering'] = 'map'
component['mode'] = 'm'
component['exec'] = None
component['macro'] = None
component['frsp'] = 3.
component['filter_name'] = None
frame['components'].append(component)

dict = {}
dict['f'] = 0.5
dict['ar'] = 0.
dict['ag'] = 153.
dict['ab'] = 54.
dict['mn'] = 0.
dict['mx'] = 0.
dict['cd'] = 0.
dict['scale'] = 'log'
frame['cargs'].append(dict)


# append to film
film['frames'].append(frame)


######################
# a frame (gas)
######################

frsp = 10.

frame = {}
frame['width'] = 512
frame['height'] = 512
frame['view'] = 'xy'
frame['tdir'] = None
frame['pfile'] = None
frame['exec'] = """nbf = nbf.select('gas');nbf.rotate(angle=-45*pi/180,axis='z');nbf.rotate(angle=-40*pi/180,axis='y');nbf = nbf.selectc(fabs(nbf.z())<0.1)"""
frame['macro'] = None
frame['components'] = []


# component '
component = {}
component['id'] = '#gas-T'
component['size'] = size
component['rendering'] = 'map'
component['mode'] = 'log10(nb.T())'
component['exec'] = None
component['macro'] = None
component['frsp'] = frsp
component['filter_name'] = None

component['to_img'] = True
component['scale'] = 'lin'
component['mx'] = 2.
component['mx'] = 5.
component['cd'] = 0.
component['palette'] = 'rainbow4'

component['img_cmd'] = []
component['img_cmd'].append(
    """ax = mt.img_mplmkaxes(img,xmin=-3,xmax=3,ymin=-3,ymax=3,xlabel=None,ylabel=None,figsize=(512,512))   """)
component['img_cmd'].append("""img = mt.img_add_axes(img,ax,size=(512,512))""")
component['img_cmd'].append("""palette = mt.img_open("colorbars/cb-T.png")""")
component['img_cmd'].append(
    """img = mt.img_append([img,palette],location='vertical')""")

frame['components'].append(component)


# component '
component = {}
component['id'] = '#gas-Fe'
component['size'] = size
component['rendering'] = 'map'
component['mode'] = 'nb.Fe()'
component['exec'] = None
component['macro'] = None
component['frsp'] = frsp
component['filter_name'] = None


component['to_img'] = True
component['scale'] = 'lin'
component['mx'] = -4.
component['mx'] = 0.
component['cd'] = 0.
component['palette'] = 'rainbow4'

component['img_cmd'] = []
component['img_cmd'].append(
    """ax = mt.img_mplmkaxes(img,xmin=-3,xmax=3,ymin=-3,ymax=3,xlabel=None,ylabel=None,figsize=(512,512))   """)
component['img_cmd'].append("""img = mt.img_add_axes(img,ax,size=(512,512))""")
component['img_cmd'].append("""palette = mt.img_open("colorbars/cb-Fe.png")""")
component['img_cmd'].append(
    """img = mt.img_append([img,palette],location='vertical')""")

frame['components'].append(component)


# component '
component = {}
component['id'] = '#gas-MgFe'
component['size'] = size
component['rendering'] = 'map'
component['mode'] = 'nb.MgFe()'
component['exec'] = None
component['macro'] = None
component['frsp'] = frsp
component['filter_name'] = None

component['to_img'] = True
component['scale'] = 'lin'
component['mx'] = -1.
component['mx'] = 1.5
component['cd'] = 0.
component['palette'] = 'rainbow4'

component['img_cmd'] = []
component['img_cmd'].append(
    """ax = mt.img_mplmkaxes(img,xmin=-3,xmax=3,ymin=-3,ymax=3,xlabel=None,ylabel=None,figsize=(512,512))   """)
component['img_cmd'].append("""img = mt.img_add_axes(img,ax,size=(512,512))""")
component['img_cmd'].append(
    """palette = mt.img_open("colorbars/cb-MgFe.png")""")
component['img_cmd'].append(
    """img = mt.img_append([img,palette],location='vertical')""")

frame['components'].append(component)


# append to film
film['frames'].append(frame)


######################
# a frame (stars)
######################

frsp = 10.

frame = {}
frame['width'] = 512
frame['height'] = 512
frame['view'] = 'xy'
frame['tdir'] = None
frame['pfile'] = None
frame['exec'] = """nbf = nbf.select('stars1');nbf.rotate(angle=-45*pi/180,axis='z');nbf.rotate(angle=-40*pi/180,axis='y');nbf = nbf.selectc(fabs(nbf.z())<0.1)"""
frame['macro'] = None
frame['components'] = []


# component '
component = {}
component['id'] = '#stars-Fe'
component['size'] = size
component['rendering'] = 'map'
component['mode'] = 'nb.Fe()'
component['exec'] = None
component['macro'] = None
component['frsp'] = frsp
component['filter_name'] = None


component['to_img'] = True
component['scale'] = 'lin'
component['mx'] = -4.
component['mx'] = 0.
component['cd'] = 0.
component['palette'] = 'rainbow4'

component['img_cmd'] = []
component['img_cmd'].append(
    """ax = mt.img_mplmkaxes(img,xmin=-3,xmax=3,ymin=-3,ymax=3,xlabel=None,ylabel=None,figsize=(512,512))   """)
component['img_cmd'].append("""img = mt.img_add_axes(img,ax,size=(512,512))""")
component['img_cmd'].append("""palette = mt.img_open("colorbars/cb-Fe.png")""")
component['img_cmd'].append(
    """img = mt.img_append([img,palette],location='vertical')""")

frame['components'].append(component)


# component '
component = {}
component['id'] = '#stars-MgFe'
component['size'] = size
component['rendering'] = 'map'
component['mode'] = 'nb.MgFe()'
component['exec'] = None
component['macro'] = None
component['frsp'] = frsp
component['filter_name'] = None

component['to_img'] = True
component['scale'] = 'lin'
component['mx'] = -1.
component['mx'] = 1.5
component['cd'] = 0.
component['palette'] = 'rainbow4'

component['img_cmd'] = []
component['img_cmd'].append(
    """ax = mt.img_mplmkaxes(img,xmin=-3,xmax=3,ymin=-3,ymax=3,xlabel=None,ylabel=None,figsize=(512,512))   """)
component['img_cmd'].append("""img = mt.img_add_axes(img,ax,size=(512,512))""")
component['img_cmd'].append(
    """palette = mt.img_open("colorbars/cb-MgFe.png")""")
component['img_cmd'].append(
    """img = mt.img_append([img,palette],location='vertical')""")


frame['components'].append(component)


# append to film
film['frames'].append(frame)


# component '
component = {}
component['id'] = '#stars-MgFevsFe'
component['size'] = (1.0, 1.0)
component['rendering'] = 'map'
component['mode'] = 'm'
component['exec'] = None
component['macro'] = 'MgFevsFe.py'
component['frsp'] = frsp
component['filter_name'] = None
frame['components'].append(component)

component['to_img'] = True
component['scale'] = 'lin'
component['mx'] = 0
component['mx'] = 0
component['cd'] = 0.
component['palette'] = 'ramp'


component['img_cmd'] = []
component['img_cmd'].append("""figsize= (512,512)""")
component['img_cmd'].append("""mn   = 0.""")
component['img_cmd'].append("""mx   = 0.""")
component['img_cmd'].append("""cd   = 0.""")
component['img_cmd'].append("""scale= 'lin'""")
component['img_cmd'].append("""xmin = -4.0""")
component['img_cmd'].append("""xmax =    0.""")
component['img_cmd'].append("""ymin = -1.0""")
component['img_cmd'].append("""ymax =  1.0""")
component['img_cmd'].append("""palette = 'rainbow4'""")
component['img_cmd'].append("""log=None""")
component['img_cmd'].append("""xlabel = r'$\\rm{[Fe]}$' """)
component['img_cmd'].append("""ylabel = r'$\\rm{[Mg/Fe]}$' """)
component['img_cmd'].append(
    """img = mt.img_add_mplaxes(img,mn=mn,mx=mx,cd=cd,scale=scale,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,palette=palette,log=log,xlabel=xlabel,ylabel=ylabel,figsize=figsize)""")
component['img_cmd'].append("""palette = mt.img_open("colorbars/cb-Fe.png")""")
component['img_cmd'].append(
    """img = mt.img_append([img,palette],location='vertical')""")

frame['components'].append(component)

# append to film
film['frames'].append(frame)
