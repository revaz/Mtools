# -*- coding: iso-8859-1 -*-

img = imgs[0]

palette_w = 5
img_w = img.size[0]
img_h = img.size[1]
# add right
img = img_add_thumbnail(
    img,
    'palette.png',
    pos=(
        img_w -
        palette_w,
        0),
    size=(
        palette_w,
        img_h),
    rotate=90)
