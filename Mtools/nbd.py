from optparse import OptionParser


from pNbody import *

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import copy


def txt2nbd(
        text=None,
        width=512,
        height=None,
        textsize=60,
        font='kidpr.ttf',
        fdz=1,
        nlayers=1,
        sph=False,
        outputimagefile=None,
        outputfile=None,
        mass=None):
    """
    width			: text width
    height		: text height
    textsize		: text size
    text			: text
    font			: name of ttf file
    outputimagefile	: output image file name
    fdz			: factor for text thickness
    nlayers		: number of layers
    sph			: use sph
    outputfile		: output file name
    """

    bg = (256, 256, 256)

    if height is None:
        height = textsize

    ###################################
    # create and write in an image
    ###################################

    # create an empty image
    img = Image.new('RGB', (width, height), bg)

    # write text
    font = ImageFont.truetype(font, textsize)

    if text is not None:
        draw = ImageDraw.Draw(img)
        draw.text((0, 0), "%s" % text, fill=(0, 0, 0), font=font)

    # if the image needs to be saved
    if outputimagefile is not None:
        img.save(outputimagefile)

    ###################################
    # create the nbody object
    ###################################

    r, g, b = img.split()

    r = fromstring(r.tostring(), uint8)
    g = fromstring(g.tostring(), uint8)
    b = fromstring(b.tostring(), uint8)

    r.shape = (img.size[1], img.size[0])
    g.shape = (img.size[1], img.size[0])
    b.shape = (img.size[1], img.size[0])

    # libutil.mplot(b)

    x = []
    y = []
    z = []
    f = []

    q = b

    for ix in range(q.shape[0]):
        for iy in range(q.shape[1]):
            if q[ix, iy] < 255.:

                x.append(ix)
                y.append(iy)
                z.append(0)

                f.append(1.)

    x = array(x)
    y = array(y)
    z = array(z)
    f = array(f)

    # create nbody object
    pos = transpose(array([x, y, z]))

    n = len(pos)
    if mass is None:
        mass = ones([n]) / float(n)
    else:
        mass = ones([n]) * mass

    nb = Nbody(status='new', pos=pos, mass=mass, ftype='gadget')
    nb.set_tpe(0)

    nb.rotate(axis='z', angle=-pi / 2.)

    ###################################
    # add layers
    ###################################

    dz = (nb.y().max() - nb.y().min()) / (50 * fdz)

    if nlayers > 1:

        for i in range(nlayers):
            nbc = copy.copy(nb)
            nbc.translate([0, 0, dz])
            nb.append(nbc)

    nb.pos = nb.pos + random.random((nb.nbody, 3)) * dz

    nb.cmcenter()

    ###################################
    # compute sph
    ###################################

    if sph:

        Density, Hsml = nb.ComputeDensityAndHsml()

        nb.u = ones(nb.nbody)
        nb.rho = ones(nb.nbody)
        nb.rsp = Hsml

    if outputfile is not None:
        nb.rename(outputfile)
        nb.write()

    return nb
