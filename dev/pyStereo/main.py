import sys
import os
import string

from numarray import *

import Image
import ImageDraw
import ImageFont
import ImagePalette
import ImageTk


def logfilter(mat, cd):
    mn = 0.
    mx = 255.
    mat = 255. * log(1. + (mat - mn) / (cd)) / log(1. + (mx - mn) / (cd))
    return mat


def linfilter(mat, mn, mx):
    mat = clip(mat, mn, mx)
    mat = 255. * (mat - mn) / (mx - mn)
    return mat


def open_image(file):

    img = Image.open(file)
    r, g, b = img.split()
    r = fromstring(r.tostring(), UInt8)
    g = fromstring(g.tostring(), UInt8)
    b = fromstring(b.tostring(), UInt8)

    r.shape = (img.size[1], img.size[0])
    g.shape = (img.size[1], img.size[0])
    b.shape = (img.size[1], img.size[0])

    return r, g, b, img.size


def write_image(file, size, r, g, b, quality=75, geometry=100.):

    image_r = Image.fromstring("L", size, r)
    image_g = Image.fromstring("L", size, g)
    image_b = Image.fromstring("L", size, b)

    # merge image
    img = Image.merge("RGB", (image_r, image_g, image_b))

    # resize image
    #geometry = geometry/100.
    #shape = (int(img.size[0]*geometry),int(img.size[1]*geometry))
    #img = img.transform(shape,Image.AFFINE,(1/geometry,0.,0.,0.,1/geometry,0.),Image.NEAREST)

    img.save(file, quality=quality)


def convert_to_gray(r, g, b, f):

    r = r.astype(Float)
    g = g.astype(Float)
    b = b.astype(Float)

    rg = sqrt((r**2 + g**2 + b**2) / 3)
    gg = rg
    bg = rg

    rg = f * rg + (1. - f) * r
    gg = f * gg + (1. - f) * g
    bg = f * bg + (1. - f) * b

    rg = rg.astype(UInt8)
    gg = gg.astype(UInt8)
    bg = bg.astype(UInt8)

    return rg, gg, bg
