#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-


from pNbody import ic
from numpy import *

from optparse import OptionParser
import os


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-o",
                      action="store",
                      dest="outputname",
                      type="string",
                      default=None,
                      help="output name",
                      metavar=" NAME")

    parser.add_option("-d",
                      action="store",
                      dest="directory",
                      type="string",
                      default='snap',
                      help="directory name",
                      metavar=" NAME")

    parser.add_option("-t",
                      action="store",
                      type="string",
                      dest="ftype",
                      default='gadget',
                      help="nbody format")

    parser.add_option("--type",
                      action="store",
                      type="string",
                      dest="type",
                      default='line',
                      help="type of object")

    parser.add_option("-n",
                      action="store",
                      type="int",
                      dest="n",
                      default=10,
                      help="number of files to write")

    (options, args) = parser.parse_args()

    files = args

    return files, options


files, opt = parse_options()


for i in range(opt.n):
    nb = ic.line(ftype=opt.ftype)
    name = "%s%03d" % (os.path.join(opt.directory, opt.outputname), i)
    nb.rename(name)
    nb.write()
    print(name)
