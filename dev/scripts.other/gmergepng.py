#!/usr/bin/env python

import os
import sys

from optparse import OptionParser

import glob

##########################################################################


def parse_options():
    ##########################################################################

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-o",
                      action="store",
                      dest="outputdirectory",
                      type="string",
                      default=None,
                      help="output directory")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        if not options.only_film:
            print("you must specify at least a filename")
            sys.exit(0)

    return args, options


##########################################################################
#
#  MAIN
#
##########################################################################

directories, opt = parse_options()

if not os.path.exists(opt.outputdirectory):
    os.mkdir(opt.outputdirectory)

i = 0
for directory in directories:

    files = sorted(glob.glob(os.path.join(directory, "*")))

    for file in files:
        name = os.path.join(opt.outputdirectory, "%08d.png" % i)

        # create a soft link
        os.symlink(os.path.join('..', file), name)
        i = i + 1
