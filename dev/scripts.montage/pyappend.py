#!/usr/bin/env python


'''
Combine images


Yves Revaz
mar oct 24 16:35:46 CEST 2006
'''

from numpy import *
from optparse import OptionParser
import string
import sys
import os

import Image
#import ImageDraw
#import ImageFont
#import ImagePalette
#import ImageTk


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-l",
                      action="store",
                      dest="left",
                      type="string",
                      default=None,
                      help="left image(s)")

    parser.add_option("-r",
                      action="store",
                      dest="right",
                      type="string",
                      default=None,
                      help="right image(s)")

    parser.add_option("-o",
                      action="store",
                      dest="outputfile",
                      type="string",
                      default=None,
                      help="oputput file",
                      metavar=" FILE NAME")

    (options, args) = parser.parse_args()

    files = args

    return files, options

#############################
# main
#############################


# get options
files, opt = parse_options()


#############################
# open file
#############################


# left image
imgl = Image.open(opt.left)

# right image
imgr = Image.open(opt.right)

if imgl.size != imgr.size:
    raise Exception("Images must have the same shape !")


# new image
img = Image.new('RGB', (2 * imgr.size[0], imgr.size[1]), (255, 255, 255))


W, H = img.size
w, h = imgr.size

img.paste(imgr, (0, 0, w, h))
img.paste(imgl, (0 + w, 0, w + w, h))


img.save(opt.outputfile)
