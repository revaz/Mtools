#!/usr/bin/env python

import glob
import os
import sys

from optparse import OptionParser

import Image


##########################################################################
def parse_options():
    ##########################################################################

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-d", "--dir",
                      action="store",
                      dest="dir",
                      type="string",
                      default="tmp",
                      help="directory for png files")

    parser.add_option("--istart",
                      action="store",
                      dest="istart",
                      type="int",
                      default=0,
                      help="first number of frame")

    parser.add_option("--ratio",
                      action="store",
                      dest="ratio",
                      type="float",
                      default=None,
                      help="screen ratio")

    parser.add_option("--maxwidth",
                      action="store",
                      dest="maxwidth",
                      type="int",
                      default=2000,
                      help="max image width")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        if not options.only_film:
            print "you must specify at least a filename"
            sys.exit(0)

    return args, options

##########################################################################
#
#  MAIN
#
##########################################################################


files, options = parse_options()


directory1 = files[0]
directory2 = files[1]


files1 = glob.glob("%s/*" % (directory1))
files2 = glob.glob("%s/*" % (directory2))

files1.sort()
files2.sort()

if len(files1) != len(files2):
    raise "files in dir1 differents than in dir 2"

if not os.path.exists(options.dir):
    os.mkdir(options.dir)

i = -1 + options.istart

for n in range(len(files1)):

    i = i + 1

    f1 = files1[n]
    f2 = files2[n]
    f3 = "%s/%08d.png" % (options.dir, i)
    print f3

    #os.system("convert +append %s %s %s"%(f1,f2,f3))

    # left image
    imgl = Image.open(f1)

    # right image
    imgr = Image.open(f2)

    if imgl.size != imgr.size:
        raise "Images must have the same shape !"

    if options.ratio is not None:

        # set the final size
        w = imgl.size[0]
        h = imgl.size[1]

        f = options.ratio * float(h) / float(w)
        dw = int(0.5 * w * (f - 1))

        W = 4 * dw + 2 * w
        size = (W, h)

        # new image
        img = Image.new('RGB', size, (0, 0, 0))

        img.paste(imgl, (dw, 0, dw + w, h))
        img.paste(imgr, (W - dw - w, 0, W - dw, h))

    else:

        W, H = 2 * imgr.size[0], imgr.size[1]
        w, h = imgr.size

        if W > options.maxwidth:  # mplayer limitation
            W = options.maxwidth

            f = float(options.maxwidth) / (2 * imgr.size[0])
            newsize = (int(imgr.size[0] * f), int(imgr.size[1] * f))
            imgr = imgr.resize(newsize)
            imgl = imgl.resize(newsize)

            W, H = 2 * imgr.size[0], imgr.size[1]
            w, h = imgr.size

        # new image
        img = Image.new('RGB', (W, H), (0, 0, 0))

        img.paste(imgr, (0, 0, w, h))
        img.paste(imgl, (0 + w, 0, w + w, h))

    img.save(f3)
