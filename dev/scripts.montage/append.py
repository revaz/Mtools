#!/usr/bin/env python

import glob
import os
import sys

from optparse import OptionParser


##########################################################################
def parse_options():
    ##########################################################################

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-d", "--dir",
                      action="store",
                      dest="dir",
                      type="string",
                      default="tmp",
                      help="directory for png files")

    parser.add_option("--istart",
                      action="store",
                      dest="istart",
                      type="int",
                      default=0,
                      help="first number of frame")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        if not options.only_film:
            print("you must specify at least a filename")
            sys.exit(0)

    return args, options

##########################################################################
#
#  MAIN
#
##########################################################################


files, options = parse_options()


directory1 = files[0]
directory2 = files[1]


files1 = glob.glob("%s/*" % (directory1))
files2 = glob.glob("%s/*" % (directory2))

files1.sort()
files2.sort()

if len(files1) != len(files2):
    raise Exception("files in dir1 differents than in dir 2")

if not os.path.exists(options.dir):
    os.mkdir(options.dir)

i = -1 + options.istart

for n in range(len(files1)):

    i = i + 1

    f1 = files1[i]
    f2 = files2[i]

    f3 = "%s/%06d.png" % (options.dir, i)
    print(f3)
    os.system("convert +append %s %s %s" % (f1, f2, f3))
    # print "convert +append %s %s %s"%(f1,f2,f3)
