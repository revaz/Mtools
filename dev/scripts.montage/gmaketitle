#!/usr/bin/env python


import Image
import ImageDraw
import ImageFont
import ImagePalette
import ImageTk
import ImageFont

import os
import sys

from optparse import OptionParser


def fct(x):
    return x * 0.5


##########################################################################
def parse_options():
    ##########################################################################

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-q", "--quality",
                      action="store",
                      dest="quality",
                      type="int",
                      default=5,
                      help="quality 1,2,3,4,5")

    parser.add_option("--fps",
                      action="store",
                      dest="fps",
                      type="int",
                      default=24,
                      help="frame per second")

    parser.add_option("--t1",
                      action="store",
                      dest="t1",
                      type="float",
                      default=1,
                      help="duration of first phase")

    parser.add_option("--t2",
                      action="store",
                      dest="t2",
                      type="float",
                      default=4,
                      help="duration of second phase")

    parser.add_option("--t3",
                      action="store",
                      dest="t3",
                      type="float",
                      default=1,
                      help="duration of third phase")

    parser.add_option("-m", "--mode",
                      action="store",
                      dest="mode",
                      type="string",
                      default=None,
                      help="image mode : L, P or RGB")

    parser.add_option("--nofilm",
                      action="store_true",
                      dest="nofilm",
                      default=False,
                      help="do not create the film (extract image only)")

    parser.add_option("-k", "--keep",
                      action="store_true",
                      dest="keep",
                      default=False,
                      help="keep png images")

    parser.add_option("-d", "--dir",
                      action="store",
                      dest="dir",
                      type="string",
                      default="tmp",
                      help="directory for png files")

    parser.add_option("--dylab",
                      action="store",
                      dest="dylab",
                      type="int",
                      default=0,
                      help="size of the label")

    parser.add_option("--width",
                      action="store",
                      dest="width",
                      type="int",
                      default=None,
                      help="Width of the final image")

    parser.add_option("--height",
                      action="store",
                      dest="height",
                      type="int",
                      default=None,
                      help="Height of the final image")

    parser.add_option("-o",
                      action="store",
                      dest="outputfile",
                      type="string",
                      default=None,
                      help="output file name")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        if not options.only_film:
            print "you must specify at least a filename"
            sys.exit(0)

    return args, options


##########################################################################
#
#  MAIN
#
##########################################################################

files, options = parse_options()
file = files[0]

mode = options.mode
fps = options.fps
d1 = options.t1
d2 = options.t2
d3 = options.t3
n1 = int(fps * d1)
n2 = int(fps * d2)
n3 = int(fps * d3)


image = Image.open(file)

width = image.size[0]
height = image.size[1]

if (options.width is not None) and (options.height is not None):
    if (options.width != width) and (options.height != height):
        newimg = Image.new('RGB', (options.width, options.height), (0, 0, 0))

        dw = int(0.5 * (options.width - width))
        dh = int(0.5 * (options.height - height))

        newimg.paste(image, (dw, dh, dw + width, dh + height))
        image = newimg


if options.dylab != 0:
    width = image.size[0]
    height = image.size[1]
    shapezoom = (width, height + options.dylab)
    image = image.transform(shapezoom, Image.AFFINE,
                            (1, 0., 0., 0., 1, -options.dylab), Image.BICUBIC)


if mode is not None:
    image = image.convert(options.mode)


# output file
if options.outputfile is None:
    options.outputfile = "%s.avi" % os.path.splitext(os.path.basename(file))[0]


if not os.path.exists(options.dir):
    os.mkdir(options.dir)


j = 0

for i in range(n1):

    f = float(i) / n1
    fileout = "%s/%08d.png" % (options.dir, j)
    newimage = Image.eval(image, lambda x: x * f)
    newimage.save(fileout)
    j = j + 1


for i in range(n2):

    f = 1
    fileout = "%s/%08d.png" % (options.dir, j)
    newimage = Image.eval(image, lambda x: x * f)
    newimage.save(fileout)
    j = j + 1

for i in range(n3):

    f = 1 - float(i) / n3
    fileout = "%s/%08d.png" % (options.dir, j)
    newimage = Image.eval(image, lambda x: x * f)
    newimage.save(fileout)
    j = j + 1


####################################
# creation du film mpeg
####################################


if options.quality == 1:
    ovc = "-ovc lavc -lavcopts vcodec=mpeg4:vbitrate=256"
    oac = "-oac copy"

elif options.quality == 2:
    ovc = "-ovc lavc -lavcopts vcodec=mpeg4:vbitrate=1600"
    oac = "-oac copy"

elif options.quality == 3:
    ovc = "-ovc lavc -lavcopts vcodec=mpeg4:vbitrate=2400"
    oac = "-oac copy"

elif options.quality == 4:
    ovc = "-ovc lavc -lavcopts vcodec=mpeg4:vbitrate=8000"
    oac = "-oac copy"

else:
    ovc = "-ovc copy"
    oac = "-oac copy"


cmd = """mencoder mf://%s/*.png  -mf type=png:fps=%d %s %s -o %s""" % (
    options.dir, options.fps, ovc, oac, options.outputfile)


if not options.nofilm:
    print cmd
    os.system(cmd)

if not options.keep:
    os.system('rm %s/*' % (options.dir))
    os.removedirs('%s' % (options.dir))
