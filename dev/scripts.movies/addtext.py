#!/usr/bin/env python


import string
from numpy import *

import sys
import os
import string
import math

from optparse import OptionParser

import glob

import Image
import ImageDraw
import ImageFont
import ImagePalette
import ImageTk
import ImageFont


from pNbody import *
import time

##########################################################################


def parse_options():
    ##########################################################################

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-o",
                      action="store",
                      dest="outputfile",
                      type="string",
                      default=None,
                      help="output file name")

    parser.add_option("--text",
                      action="store",
                      dest="text",
                      type="string",
                      default=None,
                      help="text")

    parser.add_option("--text_x",
                      action="store",
                      type='float',
                      dest="text_x",
                      default=None,
                      help="text relative position in x")

    parser.add_option("--text_y",
                      action="store",
                      type='float',
                      dest="text_y",
                      default=None,
                      help="text relative position in y")

    parser.add_option("--text_size",
                      action="store",
                      dest="text_size",
                      type="int",
                      default=8,
                      help="text size")

    parser.add_option("--text_font",
                      action="store",
                      dest="text_font",
                      type="string",
                      default="kidprbol.ttf",
                      help="text font")

    parser.add_option("--text_color",
                      action="store",
                      dest="text_color",
                      type="string",
                      default='(255,255,255)',
                      help="text color")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        if not options.only_film:
            print("you must specify at least a filename")
            sys.exit(0)

    return args, options


##########################################################################
#
#  MAIN
#
##########################################################################


files, opt = parse_options()


FONT_PATH = os.path.join(NBODYPATH, 'fonts')
FFONT = os.path.join(FONT_PATH, opt.text_font)
font = ImageFont.truetype(FFONT, opt.text_size)


exec("opt.text_color = %s" % opt.text_color)

for file in files:

    print(file)

    img = Image.open(file)
    img = img.convert('RGB')

    isize = img.size
    width = isize[0]
    height = isize[1]

    # add a text
    if opt.text is not None:
        xy = font.getsize(opt.text)

        if (opt.text_x is not None) and (opt.text_y is not None):
            x = width * opt.text_x - xy[0] / 2
            y = height - height * opt.text_y - xy[1] / 2
        else:
            x = 1 * xy[1]
            y = height - xy[1] - 1 * xy[1]

        postext = (x, y)
        draw = ImageDraw.Draw(img)
        draw.text(postext, opt.text, fill=opt.text_color, font=font)

    if opt.outputfile is not None:
        img.save(opt.outputfile)
    else:
        img.save(file)
