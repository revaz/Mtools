#!/usr/bin/env python


import string
from numpy import *

import sys
import os
import string
import math

from optparse import OptionParser

import glob

import Image
import ImageDraw
import ImageFont
import ImagePalette
import ImageTk
import ImageFont


from pNbody import libutil
import time

##########################################################################


def parse_options():
    ##########################################################################

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-d", "--dir",
                      action="store",
                      dest="dir",
                      type="string",
                      default="tmp",
                      help="directory for png files")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        if not options.only_film:
            print("you must specify at least a filename")
            sys.exit(0)

    return args, options


##########################################################################
#
#  MAIN
#
##########################################################################


dirs, options = parse_options()


if not os.path.exists(options.dir):
    os.mkdir(options.dir)


nd = len(dirs)
nf = len(glob.glob(os.path.join(dirs[0], '*')))


img_lst = {}

i = 0
for idir in dirs:

    files = sorted(glob.glob(os.path.join(idir, '*')))

    img_lst[i] = files

    i = i + 1


# open first image and check size
img = Image.open(img_lst[0][0])
isize = img.size
fsize = (img.size[0] * nd, img.size[1])


k = 0
for i in range(nf):

    # create an empty image
    img = Image.new('RGB', (fsize[0], fsize[1]), (0, 0, 0))

    for j in range(nd):
        print((img_lst[j][i]))
        imgi = Image.open(img_lst[j][i])

        # paste vignette
        x = j * isize[0]
        y = isize[1] - imgi.size[1]
        dx = imgi.size[0]
        dy = imgi.size[1]
        box = (x, y, x + dx, y + dy)

        img.paste(imgi, box)

    # create final image
    outputfile = os.path.join(options.dir, '%08d.png' % k)
    img.save(outputfile)
    k = k + 1
