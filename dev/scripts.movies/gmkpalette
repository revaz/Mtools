#!/usr/bin/python


import pNbody
from pNbody import Movie
from pNbody import *
from pNbody.palette import *
from pNbody import cosmo

import string
from numpy import *
import Image
import ImageDraw
import ImageFont
import ImagePalette
import ImageTk
import ImageFont
import sys
import os
import string
import getopt
import math

from optparse import OptionParser


##########################################################################
def parse_options():
    ##########################################################################

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-q", "--quality",
                      action="store",
                      dest="quality",
                      type="int",
                      default=5,
                      help="quality 1,2,3,4,5")

    parser.add_option("-m", "--mode",
                      action="store",
                      dest="mode",
                      type="string",
                      default=None,
                      help="image mode : L, P or RGB")

    parser.add_option("--rgb",
                      action="store_true",
                      dest="rgb",
                      default=False,
                      help="rgb mode")

    parser.add_option("-o",
                      action="store",
                      dest="outputname",
                      type="string",
                      default=None,
                      help="output file name")

    parser.add_option("-p", "--palette",
                      action="store",
                      dest="palette",
                      type="string",
                      default="ramp",
                      help="color palette")

    parser.add_option("--width",
                      action="store",
                      dest="width",
                      type="int",
                      default=256,
                      help="palette width")

    parser.add_option("--height",
                      action="store",
                      dest="height",
                      type="int",
                      default=256,
                      help="palette height")

    parser.add_option("--rotation",
                      action="store",
                      dest="rotation",
                      type="float",
                      default=0,
                      help="rotaion")

    (options, args) = parser.parse_args()

    return args, options


##########################################################################
#
#  MAIN
#
##########################################################################


files, opt = parse_options()


# create a palette object
palette = Palette()
pth = os.path.join(PALETTEDIR, opt.palette)
palette.read(pth)


# create the data
x = (arange(opt.width)).astype(float)
x = 255 * (x / max(x))

data = zeros((opt.width, opt.height))
data.transpose()

for j in range(opt.height):
    data[:, j] = x

data = transpose(data)

data = data.astype(uint8)
image = Image.fromstring("P", (data.shape[0], data.shape[1]), data)
image = image.rotate(90 + opt.rotation)


# include the palette
if not opt.rgb:
    image.putpalette(palette.palette)

if opt.mode is not None:
    image = image.convert(opt.mode)


# save it
if opt.outputname is not None:
    image.save(opt.outputname)
