#!/usr/bin/env python


import string
from numpy import *

import sys
import os
import string
import math

from optparse import OptionParser

import glob

import Image
import ImageDraw
import ImageFont
import ImagePalette
import ImageTk
import ImageFont


from pNbody import libutil
import time

##########################################################################


def parse_options():
    ##########################################################################

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-o",
                      action="store",
                      dest="outputfile",
                      type="string",
                      default=None,
                      help="output file name")

    parser.add_option("--top",
                      action="store",
                      dest="top",
                      type="string",
                      default=None,
                      help="top file name")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        if not options.only_film:
            print("you must specify at least a filename")
            sys.exit(0)

    return args, options


##########################################################################
#
#  MAIN
#
##########################################################################


files, opt = parse_options()


# open top file
timg = Image.open(opt.top)
tsize = timg.size


for file in files:

    print(file)

    imgi = Image.open(file)
    isize = imgi.size

    if isize[0] != tsize[0]:
        raise Exception("images must have the same width")

    img = Image.new('RGB', (isize[0], isize[1] + tsize[1]), (0, 0, 0))

    # paste vignette
    x = 0
    y = tsize[1]
    dx = isize[0]
    dy = isize[1]
    box = (x, y, x + dx, y + dy)
    img.paste(imgi, box)

    x = 0
    y = 0
    dx = tsize[0]
    dy = tsize[1]
    box = (x, y, x + dx, y + dy)
    img.paste(timg, box)

    if opt.outputfile is not None:
        img.save(opt.outputfile)
    else:
        img.save(file)
