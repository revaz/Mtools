# -*- coding: utf-8 -*-


opt.text = """'a = %4.2f z = %5.2f'%(header['TIME'],(1/header['TIME'])-1)"""
opt.textsize = 18

# comp1
f1 = 0.1		# 1
ar1 = 58.
ag1 = 119.
ab1 = 255.

# comp2
f2 = 1e-5
ar2 = 255
ag2 = 0.
ab2 = 0.

# comp3
f3 = 0  # 1e2
ar3 = 0.
ag3 = 153.
ab3 = 54.

# comp4
f4 = 0.
ar4 = 100.
ag4 = 0.
ab4 = 0.

# comp5
f5 = 1e-4
ar5 = 255.
ag5 = 0.
ab5 = 0.

# comp6
f6 = 0.
ar6 = 255.
ag6 = 0.
ab6 = 0.


#
mx = 0.5		# max for 255
fmx = 0.001		# relative turnoff
