#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

import sys

import Image
import ImageDraw
import ImageFont

from optparse import OptionParser


##########################################################################
def parse_options():
    ##########################################################################

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-m", "--mode",
                      action="store",
                      dest="mode",
                      type="string",
                      default=None,
                      help="image mode : L, P or RGB")

    parser.add_option("-p", "--parameterfile",
                      action="store",
                      dest="parameterfile",
                      type="string",
                      default=None,
                      help="parameter file")

    parser.add_option("-o",
                      action="store",
                      dest="outputfile",
                      type="string",
                      default=None,
                      help="output file name")

    (options, args) = parser.parse_args()

    return args, options

##########################################################################


def img_add_text(
    img, text=None, color=(
        0, 0, 0), pos=(
            0, 0), size=32, font=None, center=None):
    '''
    text  : text
    color : color of the border
    pos   : text position
    size  : text size
    font  : text font
    center : horizontal,vertical
    '''

    if text is None:
        return img

    if font is None:
        raise Exception("you must specify a font name")

    font = ImageFont.truetype(font, size)

    if center == 'horizontal':
        pos = (img.size[0] / 2 - font.getsize(text)[0] / 2, pos[1])

    elif center == 'vertical':
        pos = (pos[0], img.size[1] / 2 - font.getsize(text)[1] / 2)

    elif center == "both":
        pos = (img.size[0] / 2 - font.getsize(text)[0] / 2, pos[1])
        pos = (pos[0], img.size[1] / 2 - font.getsize(text)[1] / 2)

    draw = ImageDraw.Draw(img)
    draw.text(pos, text, fill=color, font=font)

    return img


def img_add_borders(img, color=(0, 0, 0), fsize=None, location=None, width=0):
    '''
    color : color of the border
    fsize : final size of the image
            in this case, the border is computed automatically and the image centred
    locate : location of the border : top,bottom,left,right
    width  : width of the border
    '''

    h, w = img.size

    # set size of the new image
    if fsize is not None:

        # compute position
        x = (fsize[0] - img.size[0]) / 2
        y = (fsize[1] - img.size[1]) / 2

    elif location == 'top':
        fsize = (img.size[0], img.size[1] + width)
        x = 0
        y = width

    elif location == 'bottom':
        fsize = (img.size[0], img.size[1] + width)
        x = 0
        y = 0

    elif location == 'left':
        fsize = (img.size[0] + width, img.size[1])
        x = width
        y = 0

    elif location == 'right':
        fsize = (img.size[0] + width, img.size[1])
        x = 0
        y = 0

    else:
        raise Exception("add_borders : argument must be either fsize (tuble) or location (top,bottom,left,right)")

    dx = img.size[0]
    dy = img.size[1]
    box = (x, y, x + dx, y + dy)
    imgb = Image.new(img.mode, fsize, color)

    # paste image
    imgb.paste(img, box)
    img = imgb

    return img


def img_add_thumbnail(img, thumbnail, pos=(0, 0), size=None, rotate=None):
    '''
    thumbnail : name of the thumbnail
    pos  : position of the thumbnail
    size : final size of the thumbnail
    '''

    imga = Image.open(thumbnail)

    if rotate is not None:
        imga = imga.rotate(rotate)

    if size is None:
        size = imga.size

    # resize imga if needed
    if (imga.size[0] != size[0]) or (imga.size[1] != size[0]):
        imga = imga.resize(size)

    x = pos[0]
    y = pos[1]
    box = (x, y, x + size[0], y + size[1])
    img.paste(imga, box)

    return img


##########################################################################
#
#  MAIN
#
##########################################################################


files, opt = parse_options()


file = files[0]

# open the image
img = Image.open(file)

# set the output type
if opt.mode is not None:
    img = img.convert(opt.mode)


# apply transformation

###########################
# add text (not obvious, need polices)
###########################

img = img_add_text(
    img,
    "hello world",
    color=(
        256,
        0,
        0),
    font="/home/revaz/pgm/python/pNbody-4.0/fonts/Courier_New_Bold.ttf",
    size=16,
    center='horizontal')

###########################
# add border
###########################

img = img_add_borders(
    img, fsize=(
        410, 256), color=(
            255, 255, 0))		# set final size and center image
#img = img_add_borders(img,location='right',width=56,color=(255,255,255))


###########################
# add palette / image
###########################

#img = img_add_thumbnail(img,'palette.png',pos=(0,0),size=(img.size[0],50),rotate=90)
img = img_add_thumbnail(
    img, 'palette.png', pos=(
        0, 0), size=(
            50, img.size[1]), rotate=90)


###########################
# crops
###########################
box = (0, 0, 256, 256)
#img = img.crop(box)

###########################
# resize
###########################
# (NEAREST,BILINEAR,BICUBIC)
#img = img.resize((512,256),Image.BICUBIC)

# remove borders


# save image
if opt.outputfile is not None:
    img.save(opt.outputfile)
