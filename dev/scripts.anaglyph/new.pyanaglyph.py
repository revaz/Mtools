#!/usr/bin/env python


'''
Combine images


Yves Revaz
mar oct 24 16:35:46 CEST 2006
'''

from numarray import *
from optparse import OptionParser
import string
import sys
import os

import Image
import ImageDraw
import ImageFont
import ImagePalette
import ImageTk

import numarray.linear_algebra as la


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)


# parser.add_option("-f",
#		  action="store",
#		  dest="inputfile",
#		  type="string",
#        	  default = None,
#		  help="input file",
#		  metavar=" FILE NAME")

    parser.add_option("-o",
                      action="store",
                      dest="outputfile",
                      type="string",
                      default=None,
                      help="oputput file",
                      metavar=" FILE NAME")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        print("you must specify a filename")
        sys.exit(0)

    files = args

    return files, options

#############################
# main
#############################


# get options
files, options = parse_options()

#inputfile  = options.inputfile
outputfile = options.outputfile


#############################
# open file
#############################


# red
img = Image.open(files[0])
r, g, b = img.split()
r = fromstring(r.tostring(), UInt8)
g = fromstring(g.tostring(), UInt8)
b = fromstring(b.tostring(), UInt8)

# make it gray
r = r.astype(Float)
g = g.astype(Float)
b = b.astype(Float)
c = sqrt(r * r / 3. + g * g / 3. + b * b / 3.)
r = c.astype(UInt8)
g = c.astype(UInt8)
b = c.astype(UInt8)

size = img.size

r.shape = (size[1], size[0])
g.shape = (size[1], size[0])
b.shape = (size[1], size[0])

# cut image

r2 = r[:, 0:size[0] / 2]
g2 = g[:, 0:size[0] / 2]
b2 = b[:, 0:size[0] / 2]

r1 = r[:, size[0] / 2:]
g1 = g[:, size[0] / 2:]
b1 = b[:, size[0] / 2:]


r1 = ravel(r1).astype(Float)
g1 = ravel(g1).astype(Float)
b1 = ravel(b1).astype(Float)
r2 = ravel(r2).astype(Float)
g2 = ravel(g2).astype(Float)
b2 = ravel(b2).astype(Float)
r = ones(len(r1), Float)
g = ones(len(r1), Float)
b = ones(len(r1), Float)

# transform into new base

A = ones((3, 3), Float)

A[0, 0] = 1.	 # yellow
A[1, 0] = 1.
A[2, 0] = 0.

A[0, 1] = 0.	 # cyan
A[1, 1] = 1.
A[2, 1] = 1.

A[0, 2] = 1.	 # rose
A[1, 2] = 0.
A[2, 2] = 1.

# inverse
B = la.inverse(A)


# make big matrix from colors
c1 = array([r1, g1, b1])
c2 = array([r2, g2, b2])


# convert into base 2
print("convert into 2")
cb1 = matrixmultiply(B, c1)
cb2 = matrixmultiply(B, c2)

# extract colors		      <----------------
yy = cb1[0]
cc = cb2[1]
rr = cb1[2]


c = array([yy, cc, rr])

# convert into base 1
print("convert into 1")
c = matrixmultiply(A, c)
r = c[0]
g = c[1]
b = c[2]


r = ravel(r.astype(UInt8))
g = ravel(g.astype(UInt8))
b = ravel(b.astype(UInt8))

# new size
size = (size[0] / 2, size[1])

image_r = Image.fromstring("L", size, r)
image_g = Image.fromstring("L", size, g)
image_b = Image.fromstring("L", size, b)

# merge image
img = Image.merge("RGB", (image_r, image_g, image_b))
img.save(outputfile)
