#!/usr/bin/env python


'''
Combine images


Yves Revaz
mar oct 24 16:35:46 CEST 2006
'''

from numarray import *
from optparse import OptionParser
import string
import sys
import os

import Image
import ImageDraw
import ImageFont
import ImagePalette
import ImageTk

from Nbody import *


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)


# parser.add_option("-f",
#		  action="store",
#		  dest="inputfile",
#		  type="string",
#        	  default = None,
#		  help="input file",
#		  metavar=" FILE NAME")

    parser.add_option("-o",
                      action="store",
                      dest="outputfile",
                      type="string",
                      default=None,
                      help="oputput file",
                      metavar=" FILE NAME")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        print("you must specify a filename")
        sys.exit(0)

    files = args

    return files, options


def logfilter(mat, cd):
    mn = 0.
    mx = 255.
    mat = 255. * log(1. + (mat - mn) / (cd)) / log(1. + (mx - mn) / (cd))
    return mat


def linfilter(mat, mn, mx):
    mat = clip(mat, mn, mx)
    mat = 255. * (mat - mn) / (mx - mn)
    return mat


#############################
# main
#############################


# get options
files, options = parse_options()

#inputfile  = options.inputfile
outputfile = options.outputfile


#############################
# open file
#############################


# red
img = Image.open(files[0])
r, g, b = img.split()
r = fromstring(r.tostring(), UInt8)
g = fromstring(g.tostring(), UInt8)
b = fromstring(b.tostring(), UInt8)


# make it gray
# only using gas

palette = Palette('grey')
i = b
#i = linfilter(i,0,128)
i = logfilter(i, 64)

r = palette.getr(i)
g = palette.getg(i)
b = palette.getb(i)


# make it gray
#r = r.astype(Float)
#g = g.astype(Float)
#b = b.astype(Float)
#c = sqrt(r*r+g*g+b+b)
#r = c.astype(UInt8)
#g = c.astype(UInt8)
#b = c.astype(UInt8)

size = img.size

r.shape = (size[1], size[0])
g.shape = (size[1], size[0])
b.shape = (size[1], size[0])

# cut image

r2 = r[:, 0:size[0] / 2]
g2 = g[:, 0:size[0] / 2]
b2 = b[:, 0:size[0] / 2]

r1 = r[:, size[0] / 2:]
g1 = g[:, size[0] / 2:]
b1 = b[:, size[0] / 2:]

r = r1
g = g2
b = b2


r = ravel(r)
g = ravel(g)
b = ravel(b)

r = r.astype(UInt8)
g = g.astype(UInt8)
b = b.astype(UInt8)

# new size
size = (size[0] / 2, size[1])


image_r = Image.fromstring("L", size, r)
image_g = Image.fromstring("L", size, g)
image_b = Image.fromstring("L", size, b)


# merge image
img = Image.merge("RGB", (image_r, image_g, image_b))


img.save(outputfile)
