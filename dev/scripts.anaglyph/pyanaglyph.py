#!/usr/bin/env python


'''
Combine images


Yves Revaz
mar oct 24 16:35:46 CEST 2006
'''

from numarray import *
from optparse import OptionParser
import string
import sys
import os

import Image
import ImageDraw
import ImageFont
import ImagePalette
import ImageTk


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)


# parser.add_option("-f",
#		  action="store",
#		  dest="inputfile",
#		  type="string",
#        	  default = None,
#		  help="input file",
#		  metavar=" FILE NAME")

    parser.add_option("-o",
                      action="store",
                      dest="outputfile",
                      type="string",
                      default=None,
                      help="oputput file",
                      metavar=" FILE NAME")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        print("you must specify a filename")
        sys.exit(0)

    files = args

    return files, options

#############################
# main
#############################


# get options
files, options = parse_options()

#inputfile  = options.inputfile
outputfile = options.outputfile


#############################
# open file
#############################


# red
img = Image.open(files[0])
r, g, b = img.split()
r = fromstring(r.tostring(), UInt8)
g = fromstring(g.tostring(), UInt8)
b = fromstring(b.tostring(), UInt8)

size = img.size
r.shape = (size[1], size[0])
g.shape = (size[1], size[0])
b.shape = (size[1], size[0])

# cut image

r1 = r[:, 0:size[0] / 2]
g1 = g[:, 0:size[0] / 2]
b1 = b[:, 0:size[0] / 2]

r2 = r[:, size[0] / 2:]
g2 = g[:, size[0] / 2:]
b2 = b[:, size[0] / 2:]

r = r1
g = g2
b = b2


r = ravel(r)
g = ravel(g)
b = ravel(b)

# new size
size = (size[0] / 2, size[1])


#
image_r = Image.fromstring("L", size, r)
image_g = Image.fromstring("L", size, g)
image_b = Image.fromstring("L", size, b)


# merge image
img = Image.merge("RGB", (image_r, image_g, image_b))


img.save(outputfile)
