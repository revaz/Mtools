#!/usr/bin/env python

from numpy import *

import sys

import Image
import ImageDraw
import ImageFont


img1 = Image.open('../examples/right/00000050.png')
img2 = Image.open('../examples/right/00000200.png')

img1 = img1.convert('RGB')
img2 = img2.convert('RGB')


# img1
r1, g1, b1 = img1.split()
r1a = array(r1.getdata())
g1a = array(g1.getdata())
b1a = array(b1.getdata())

# img2
r2, g2, b2 = img2.split()
r2a = array(r2.getdata())
g2a = array(g2.getdata())
b2a = array(b2.getdata())

r = r1
g = g1
b = b1


#####################################################
#

n = 24 * 3

for i in range(n):

    print((i, n))

    f = i / float(n)

    ra = r1a * f + r2a * (1 - f)
    ga = g1a * f + g2a * (1 - f)
    ba = b1a * f + b2a * (1 - f)

    # put
    r.putdata(ra.tolist())
    g.putdata(ga.tolist())
    b.putdata(ba.tolist())

    img = Image.merge("RGB", (r, g, b))

    img.save('shade/%08d.png' % i)
